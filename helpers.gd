extends Node

func get_UV_coord(target : MeshInstance, position : Vector3) -> Vector2:
	var local_pos = target.to_local(position)
	var local2_pos = Vector2(local_pos.x, local_pos.z)
	return (local2_pos + (target.mesh.size / 2)) / target.mesh.size