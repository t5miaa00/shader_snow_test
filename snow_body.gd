extends MeshInstance

const RAY_LEN = 100

onready var cam = get_viewport().get_camera()
onready var height_node = $"../height_map"
onready var mat = self.get_surface_material(0)

var override_brush = false

func _ready():
	mat.set_shader_param("splat_map", height_node.get_texture())
	height_node.do_fill(1.0, true)
	mesh.subdivide_depth = 500
	mesh.subdivide_width = mesh.subdivide_depth
	#_update_brush_size(height_node.brush_size)

func _update_brush_size(value : Vector2):
	height_node.set_brush_size(value)

func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_WHEEL_UP: #increase the brush size
			_update_brush_size(height_node.brush_size * 1.1)
		elif event.button_index == BUTTON_WHEEL_DOWN: #decrease the brush size
			_update_brush_size(height_node.brush_size / 1.1)

func _physics_process(delta):
	var mouse_pos = get_viewport().get_mouse_position()
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		override_brush = false

		var r_from =          cam.project_ray_origin(mouse_pos)
		var r_to   = r_from + cam.project_ray_normal(mouse_pos) * RAY_LEN

		var d_s_state = get_world().direct_space_state
		var r_hit = d_s_state.intersect_ray(r_from, r_to)
		if r_hit && r_hit.collider.get_parent().name == "snow_body":
			height_node.do_brush(helpers.get_UV_coord(self, -r_hit.position), delta)
	if Input.is_action_just_released("brush"):
		override_brush = true

	#height_node.do_fill(0.8 * delta, override_brush)
