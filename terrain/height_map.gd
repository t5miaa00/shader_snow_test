extends Viewport

# # # # # # # # #
# Brush Strength
export(float, 1.0, 30.0) var brush_str = 30.0 setget set_brush_str, get_brush_str
func set_brush_str(value) -> void:
	brush_str = value

func get_brush_str() -> float:
	return brush_str

# # # # # # #
# Brush size
export(Vector2) var brush_size = Vector2(10.0, 10.0) setget set_brush_size, get_brush_size
func set_brush_size(value : Vector2) -> void:
	value.x = clamp(value.x, 1.0, 256.0)
	value.y = clamp(value.y, 1.0, 256.0)
	brush_size = value

	if $brush:
		print("setting rect to: ", brush_size)
		$brush.rect_size = brush_size
		print("current rect: ", $brush.rect_size)

	set_brush_pos(brush_pos)

func get_brush_size() -> Vector2:
	return brush_size

# # # # # # # # #
# Brush position
export(Vector2) var brush_pos = Vector2(1024.0, 1024.0) setget set_brush_pos, get_brush_pos
func set_brush_pos(value : Vector2) -> void:
	brush_pos = value
	var center_pos = brush_pos - (get_brush_size() * 0.5)

	if $brush:
		$brush.rect_position = center_pos

func get_brush_pos() -> Vector2:
	return brush_pos

# # # # # # #
# Apply Brush
func do_brush(uv_pos : Vector2, mult : float) -> void:
	set_brush_pos(uv_pos * size)
	$brush.material.set_shader_param("brush_str", brush_str * mult)

	$prefill.visible = false
	$brush.visible = true

	render_target_update_mode = Viewport.UPDATE_ONCE

func end_brush() -> void:
	$brush.visible = false

# # # # # # #
# Apply Fill
func do_fill(opacity : float, override_brush : bool) -> void:
	assert(opacity <= 1.0)
	$prefill.set_modulate(Color(1.0, 1.0, 1.0, opacity))
	$prefill.visible = true
	$brush.visible = ! override_brush
	render_target_update_mode = Viewport.UPDATE_ONCE

func _ready() -> void:
		$prefill.rect_size = size

		set_brush_size(brush_size)