shader_type canvas_item;

uniform float brush_str = 10.0;

void fragment() {
	float height_add = brush_str * texture(TEXTURE, UV).r;
	vec4 color = texture(SCREEN_TEXTURE, SCREEN_UV);
	
	float height = color.r;
	
	height = clamp(height + height_add, 0.0, 1.0);
	
	color.r = height;
	color.a = 1.0; // JIC
	
	COLOR = color;
}