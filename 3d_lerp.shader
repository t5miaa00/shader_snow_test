shader_type spatial;
render_mode diffuse_toon, specular_toon;

uniform float snow_amp : hint_range(0.0, 2.0) = 1.0;
uniform vec4 snow_top : hint_color = vec4(1.0,1.0,1.0,1.0);
uniform vec4 snow_bot : hint_color = vec4(0.0,0.0,0.0,1.0);
uniform sampler2D splat_map : hint_black_albedo;

uniform vec2 amp = vec2(0.2, 0.3);

float height(sampler2D splat, vec2 coord) {
	return texture(splat, coord).x * snow_amp;
}

void vertex() {
	VERTEX.y -= height(splat_map, UV) - snow_amp;
	TANGENT = -normalize(vec3(0.0, height(splat_map, UV + vec2(0.0, 0.02)) - height(splat_map, UV + vec2(0.0, -0.02)), 0.4));
	BINORMAL = -normalize(vec3(0.4, height(splat_map, UV  + vec2(0.02, 0.0)) - height(splat_map, UV  + vec2(-0.02, 0.0)), 0.0));
	NORMAL = cross(TANGENT, BINORMAL);
}

void fragment() {
	ALBEDO = mix(snow_top, snow_bot, height(splat_map, UV) / snow_amp).rgb;
}