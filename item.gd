extends KinematicBody

const GRAVITY = -9.81
var velocity = Vector3()
var max_velocity = 10
var collision

func _physics_process(delta):
	velocity.y = clamp(velocity.y + GRAVITY * delta, -max_velocity, max_velocity)
	collision = move_and_collide(velocity * delta)